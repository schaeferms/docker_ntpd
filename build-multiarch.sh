#!/bin/bash

source vars

DOCKER=$(which docker)
BUILDER=${IMAGE_NAME}"-builder"
BUILD_DATE=$(date -u '+%Y-%m-%dT%H:%M:%S%z')

BUILDER_EXISTS=$($DOCKER buildx ls| grep ${BUILDER})
if [ ! -n "$BUILDER_EXISTS" ]; then
  $DOCKER run --rm --privileged multiarch/qemu-user-static --reset -p yes
  $DOCKER buildx create --name ${BUILDER} --driver docker-container --use
else
  $DOCKER buildx use ${BUILDER}
fi

$DOCKER buildx build --platform linux/amd64,linux/arm64,linux/ppc64le,linux/s390x,linux/386,linux/arm/v7,linux/arm/v6 \
                     --build-arg BUILD_DATE=${BUILD_DATE} \
                     --tag ${CONTAINER_REGISTRY}${REGISTRY_USER}/${IMAGE_NAME}:${TAG} \
                     --push .

$DOCKER rm -f $($DOCKER ps --filter "name=buildx_buildkit" --format "{{.ID}}")

IMAGE_NAME="docker-ntpd"
