FROM alpine:latest

ARG BUILD_DATE

LABEL build_datee="${BUILD_DATE}"

RUN apk add --no-cache chrony

# Using script from: https://github.com/cturra/docker-ntp
COPY entrypoint.sh /entrypoint.sh


EXPOSE 123/udp
HEALTHCHECK CMD chronyc tracking || exit 1

ENTRYPOINT [ "/bin/sh", "/entrypoint.sh" ]
